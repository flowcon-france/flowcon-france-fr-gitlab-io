import { load } from "cheerio";
import fs from "fs";
import sharp from "sharp";
import Jimp from "jimp";

const url = "https://flowcon2025.sched.com";

const siteResponse = await fetch(url);
const siteBody = await siteResponse.text();

const site = load(siteBody);

const buildImageName = (speakerName) =>
  speakerName
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/\s+/g, "-")
    .toLowerCase();

const resizer = sharp().resize(256, 256).jpeg();

const downloadImage = async (imageUrl, imageName) => {
  if (!imageUrl) {
    return "no-picture";
  }
  const url = "http:" + imageUrl;
  const fileName = `./result/images/${imageName}.jpg`;
  try {
    const img = await Jimp.read(url);
    img.resize(256, 256).quality(60).write(fileName);
    return imageName;
  } catch (err) {
    console.log("Jimp.read " + fileName + " url " + url + " with err: " + err);
    return "no-picture";
  }
};

async function loadSession(elmt) {
  const speakers = [];
  const relativeSessionUrl = site(elmt).children("a").attr("href");
  const sessionEventUrl = url + "/" + relativeSessionUrl;

  const sessionResponse = await fetch(sessionEventUrl);
  const sessionBody = await sessionResponse.text();
  const $ = load(sessionBody);
  const title = $(".name").text().trim();

  const speakerElmts = $(".sched-person-session");
  for await (const sessionElmt of speakerElmts) {
    const speakerName = $(sessionElmt).find("div h2").text();
    const imageUrl = $(sessionElmt).find(".sched-avatar span img").attr("src");

    const speaker = {
      name: speakerName,
      title: title,
      sessionUrl: sessionEventUrl,
      imageName: await downloadImage(imageUrl, buildImageName(speakerName)),
      imageUrl: imageUrl,
    };

    speakers.push(speaker);
  }

  return speakers;
}

async function loadSpeakers() {
  const speakerPromises = [];
  site(".event").each(function (_, elmt) {
    speakerPromises.push(loadSession(elmt));
  });
  const speakers = await Promise.all(speakerPromises);
  return speakers.flat();
}

const buildTomlSpeakerList = async () => {
  const speakers = await loadSpeakers();
  let toml = "";
  speakers.forEach((speaker) => {
    toml = toml + "[[params.speakers]]\n";
    toml = toml + `    name = "${speaker.name}"\n`;
    toml = toml + `    talk = "${speaker.title}"\n`;
    toml = toml + `    photo = "${speaker.imageName}"\n`;
    toml = toml + `    url = "${speaker.sessionUrl}"\n`;
    toml = toml + "\n";
  });

  fs.writeFile("./result/speakers.toml", toml, function (err) {
    if (err) return console.log("writeFile: " + err);
  });
};

buildTomlSpeakerList();
